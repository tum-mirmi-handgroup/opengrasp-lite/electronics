<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Patch_Top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="110" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="111" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="117" name="PM_Ref" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="PF_Ref" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="WFL_Ref" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="BMP390L">
<packages>
<package name="XDCR_BMP390L">
<wire x1="-1" y1="-1" x2="-1" y2="1" width="0.127" layer="51"/>
<wire x1="-1" y1="1" x2="1" y2="1" width="0.127" layer="51"/>
<wire x1="1" y1="1" x2="1" y2="-1" width="0.127" layer="51"/>
<wire x1="1" y1="-1" x2="-1" y2="-1" width="0.127" layer="51"/>
<wire x1="-1" y1="0.7075" x2="-1" y2="1" width="0.127" layer="21"/>
<wire x1="-1" y1="-1" x2="-1" y2="-0.7075" width="0.127" layer="21"/>
<wire x1="-1" y1="1" x2="-0.9575" y2="1" width="0.127" layer="21"/>
<wire x1="0.9575" y1="1" x2="1" y2="1" width="0.127" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="0.7075" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="1" y2="-0.7075" width="0.127" layer="21"/>
<wire x1="-0.9575" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="0.9575" y2="-1" width="0.127" layer="21"/>
<wire x1="-1.25" y1="1.25" x2="-1.25" y2="-1.25" width="0.05" layer="39"/>
<wire x1="-1.25" y1="-1.25" x2="1.25" y2="-1.25" width="0.05" layer="39"/>
<wire x1="1.25" y1="-1.25" x2="1.25" y2="1.25" width="0.05" layer="39"/>
<wire x1="1.25" y1="1.25" x2="-1.25" y2="1.25" width="0.05" layer="39"/>
<circle x="-1.5875" y="0.35" radius="0.1" width="0.2" layer="21"/>
<circle x="-1.5875" y="0.35" radius="0.1" width="0.2" layer="51"/>
<text x="-1.25" y="1.35" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.25" y="-1.35" size="0.4064" layer="27" align="top-left">&gt;VALUE</text>
<smd name="3" x="-0.5" y="-0.7625" dx="0.25" dy="0.275" layer="1"/>
<smd name="4" x="0" y="-0.7625" dx="0.25" dy="0.275" layer="1"/>
<smd name="10" x="-0.5" y="0.7625" dx="0.25" dy="0.275" layer="1"/>
<smd name="9" x="0" y="0.7625" dx="0.25" dy="0.275" layer="1"/>
<smd name="5" x="0.5" y="-0.7625" dx="0.25" dy="0.275" layer="1"/>
<smd name="8" x="0.5" y="0.7625" dx="0.25" dy="0.275" layer="1"/>
<smd name="1" x="-0.7625" y="0.25" dx="0.275" dy="0.25" layer="1"/>
<smd name="2" x="-0.7625" y="-0.25" dx="0.275" dy="0.25" layer="1"/>
<smd name="7" x="0.7625" y="0.25" dx="0.275" dy="0.25" layer="1"/>
<smd name="6" x="0.7625" y="-0.25" dx="0.275" dy="0.25" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="BMP390L">
<text x="-10.16" y="13.97" size="1.778" layer="95">&gt;NAME</text>
<wire x1="-10.16" y1="12.7" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<text x="-10.16" y="-11.43" size="1.778" layer="96" align="top-left">&gt;VALUE</text>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<pin name="VDD" x="15.24" y="10.16" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDIO" x="15.24" y="7.62" length="middle" direction="pwr" rot="R180"/>
<pin name="CSB" x="-15.24" y="5.08" length="middle" direction="in"/>
<pin name="SCK" x="-15.24" y="0" length="middle" direction="in" function="clk"/>
<pin name="SDI" x="-15.24" y="-2.54" length="middle" direction="in"/>
<pin name="SDO" x="-15.24" y="-5.08" length="middle" direction="out"/>
<pin name="INT" x="15.24" y="0" length="middle" direction="out" rot="R180"/>
<pin name="VSS" x="15.24" y="-7.62" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BMP390L" prefix="U">
<description> &lt;a href="https://pricing.snapeda.com/parts/BMP390L/Bosch%20Sensortec/view-part?ref=eda"&gt;Check availability&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="BMP390L" x="0" y="0"/>
</gates>
<devices>
<device name="" package="XDCR_BMP390L">
<connects>
<connect gate="G$1" pin="CSB" pad="6"/>
<connect gate="G$1" pin="INT" pad="7"/>
<connect gate="G$1" pin="SCK" pad="2"/>
<connect gate="G$1" pin="SDI" pad="4"/>
<connect gate="G$1" pin="SDO" pad="5"/>
<connect gate="G$1" pin="VDD" pad="10"/>
<connect gate="G$1" pin="VDDIO" pad="1"/>
<connect gate="G$1" pin="VSS" pad="3 8 9"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="In Stock"/>
<attribute name="DESCRIPTION" value=" Pressure Sensor 4.35PSI ~ 18.13PSI (30kPa ~ 125kPa) Absolute - - 10-WFLGA "/>
<attribute name="MF" value="Bosch Sensortec"/>
<attribute name="MP" value="BMP390L"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/BMP390L/?ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="msrm" urn="urn:adsk.eagle:library:13039032">
<packages>
<package name="C0402" urn="urn:adsk.eagle:footprint:23121/1" library_version="311">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0603" urn="urn:adsk.eagle:footprint:23123/1" library_version="311">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805" urn="urn:adsk.eagle:footprint:23124/1" library_version="311">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206" urn="urn:adsk.eagle:footprint:23125/1" library_version="311">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C0201" urn="urn:adsk.eagle:footprint:23196/1" library_version="311">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C0805K" urn="urn:adsk.eagle:footprint:23188/1" library_version="311">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:23626/2" type="model">
<description>Chip, 1.00 X 0.50 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="C0402"/>
</packageinstances>
</package3d>
<package3d name="C0603" urn="urn:adsk.eagle:package:23616/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0603"/>
</packageinstances>
</package3d>
<package3d name="C0805" urn="urn:adsk.eagle:package:23617/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0805"/>
</packageinstances>
</package3d>
<package3d name="C1206" urn="urn:adsk.eagle:package:23618/2" type="model">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1206"/>
</packageinstances>
</package3d>
<package3d name="C0201" urn="urn:adsk.eagle:package:23690/2" type="model">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<packageinstances>
<packageinstance name="C0201"/>
</packageinstances>
</package3d>
<package3d name="C0805K" urn="urn:adsk.eagle:package:23681/2" type="model">
<description>Ceramic Chip Capacitor KEMET 0805 reflow solder
Metric Code Size 2012</description>
<packageinstances>
<packageinstance name="C0805K"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="C-EU" urn="urn:adsk.eagle:symbol:13160335/2" library_version="311">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;CAPACITANCE</text>
<text x="2.54" y="-5.08" size="1.778" layer="95">&gt;VOLTAGE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GCM?R7*" urn="urn:adsk.eagle:component:13160336/8" prefix="C" library_version="250" library_locally_modified="yes">
<description>&lt;B&gt;Multilayer Ceramic Capacitors&lt;/B&gt;
&lt;p&gt;Chip Multilayer Ceramic Capacitors for Automotive –55 to 125°C
&lt;p&gt;Sources:
&lt;p&gt;&lt;a href="https://www.murata.com/~/media/webrenewal/support/library/catalog/products/capacitor/mlcc/c03e.ashx?la=en-us"&gt; Catalogue &lt;/a&gt;
&lt;p&gt;&lt;a href="https://search.murata.co.jp/Ceramy/image/img/A01X/partnumbering_e_02.pdf"&gt; Part Numbering &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="155" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23626/2"/>
</package3dinstances>
<technologies>
<technology name="1C104JA55">
<attribute name="CAPACITANCE" value="100nF"/>
<attribute name="DIGIKEY" value="490-16433-1-ND"/>
<attribute name="TOLERANCE" value="±5%"/>
<attribute name="VOLTAGE" value="16V"/>
</technology>
<technology name="1C224KE02">
<attribute name="CAPACITANCE" value="220nF"/>
<attribute name="DIGIKEY" value="490-10671-1-ND "/>
<attribute name="TOLERANCE" value="±10%"/>
<attribute name="VOLTAGE" value="16V"/>
</technology>
<technology name="1E103KA37">
<attribute name="CAPACITANCE" value="10nF"/>
<attribute name="DIGIKEY" value="490-6043-1-ND "/>
<attribute name="TOLERANCE" value="±10%"/>
<attribute name="VOLTAGE" value="25V"/>
</technology>
<technology name="1E223KA55">
<attribute name="CAPACITANCE" value="22nF"/>
<attribute name="DIGIKEY" value="490-6044-1-ND "/>
<attribute name="TOLERANCE" value="±10% "/>
<attribute name="VOLTAGE" value="25V"/>
</technology>
<technology name="1H471KA37">
<attribute name="CAPACITANCE" value="470pF"/>
<attribute name="DIGIKEY" value="490-4913-1-ND  "/>
<attribute name="TOLERANCE" value="±10%"/>
<attribute name="VOLTAGE" value="50V"/>
</technology>
</technologies>
</device>
<device name="188" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="1C105KA64">
<attribute name="CAPACITANCE" value="1uF"/>
<attribute name="DIGIKEY" value="490-5241-1-ND"/>
<attribute name="TOLERANCE" value=" ±10%"/>
<attribute name="VOLTAGE" value="16V"/>
</technology>
<technology name="1C474KA55">
<attribute name="CAPACITANCE" value="470nF"/>
<attribute name="DIGIKEY" value="490-12589-1-ND "/>
<attribute name="TOLERANCE" value="±10% "/>
<attribute name="VOLTAGE" value="16V"/>
</technology>
<technology name="2A103KA37">
<attribute name="CAPACITANCE" value="10nF"/>
<attribute name="DIGIKEY" value="490-4781-1-ND  "/>
<attribute name="TOLERANCE" value="±10%"/>
<attribute name="VOLTAGE" value="100V"/>
</technology>
<technology name="2A223KA37">
<attribute name="CAPACITANCE" value="22nF"/>
<attribute name="DIGIKEY" value="490-4782-1-ND  "/>
<attribute name="TOLERANCE" value="±10%"/>
<attribute name="VOLTAGE" value="100V"/>
</technology>
</technologies>
</device>
<device name="21B" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23617/2"/>
</package3dinstances>
<technologies>
<technology name="0J106KE22K">
<attribute name="CAPACITANCE" value="10uF"/>
<attribute name="DIGIKEY" value="490-14361-1-ND"/>
<attribute name="TOLERANCE" value="±10%"/>
<attribute name="VOLTAGE" value="6.3V"/>
</technology>
<technology name="1C225KA64">
<attribute name="CAPACITANCE" value="2.2uF"/>
<attribute name="DIGIKEY" value="490-4786-1-ND"/>
<attribute name="TOLERANCE" value="±10%"/>
<attribute name="VOLTAGE" value="16V"/>
</technology>
<technology name="1H474KA55">
<attribute name="CAPACITANCE" value="470nF"/>
<attribute name="DIGIKEY" value="490-4788-1-ND "/>
<attribute name="TOLERANCE" value="±10% "/>
<attribute name="VOLTAGE" value="50V"/>
</technology>
<technology name="2A104KA37">
<attribute name="CAPACITANCE" value="100nF"/>
<attribute name="DIGIKEY" value="490-8051-1-ND "/>
<attribute name="TOLERANCE" value="±10% "/>
<attribute name="VOLTAGE" value="100V"/>
</technology>
</technologies>
</device>
<device name="31C" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23618/2"/>
</package3dinstances>
<technologies>
<technology name="1C106KA64">
<attribute name="CAPACITANCE" value="10uF"/>
<attribute name="DIGIKEY" value="490-14746-1-ND"/>
<attribute name="TOLERANCE" value="±10%"/>
<attribute name="VOLTAGE" value="16V"/>
</technology>
<technology name="1H225KA55">
<attribute name="CAPACITANCE" value="2.2uF"/>
<attribute name="DIGIKEY" value="490-11169-1-ND"/>
<attribute name="TOLERANCE" value="±10%"/>
<attribute name="VOLTAGE" value="50V"/>
</technology>
</technologies>
</device>
<device name="31M" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23618/2"/>
</package3dinstances>
<technologies>
<technology name="2A224KA37">
<attribute name="CAPACITANCE" value="220nF"/>
<attribute name="DIGIKEY" value="490-4975-1-ND "/>
<attribute name="TOLERANCE" value="±10% "/>
<attribute name="VOLTAGE" value="100V"/>
</technology>
</technologies>
</device>
<device name="033" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23690/2"/>
</package3dinstances>
<technologies>
<technology name="1E471KA03">
<attribute name="CAPACITANCE" value="470pF"/>
<attribute name="DIGIKEY" value="490-17744-1-ND  "/>
<attribute name="TOLERANCE" value="±10% "/>
<attribute name="VOLTAGE" value="25V"/>
</technology>
</technologies>
</device>
<device name="21A" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23681/2"/>
</package3dinstances>
<technologies>
<technology name="2A224KAC5K">
<attribute name="CAPACITANCE" value="220nF"/>
<attribute name="DIGIKEY" value="490-8306-1-ND"/>
<attribute name="TEMP_COEF" value="X7R"/>
<attribute name="TOLERANCE" value="±10%"/>
<attribute name="VOLTAGE" value="100V"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="msrm">
<description>Generated from &lt;b&gt;V2_Resistive2Pin.sch&lt;/b&gt;&lt;p&gt;
by exp-lbrs.ulp</description>
<packages>
<package name="5037630491" urn="urn:adsk.eagle:footprint:32279484/1">
<wire x1="3.9" y1="4.86" x2="-3.9" y2="4.86" width="0.0762" layer="21"/>
<wire x1="-4.3" y1="3.48" x2="-4.3" y2="2.48" width="0.0762" layer="51"/>
<wire x1="4.3" y1="3.48" x2="4.3" y2="2.48" width="0.0762" layer="51"/>
<wire x1="-3.9" y1="2.48" x2="-4.3" y2="2.48" width="0.0762" layer="51"/>
<wire x1="-3.9" y1="3.48" x2="-4.3" y2="3.48" width="0.0762" layer="51"/>
<wire x1="3.9" y1="3.48" x2="4.3" y2="3.48" width="0.0762" layer="51"/>
<wire x1="3.9" y1="2.48" x2="4.3" y2="2.48" width="0.0762" layer="51"/>
<wire x1="-3.9" y1="0" x2="-3.9" y2="2.2" width="0.0762" layer="21"/>
<wire x1="-3.9" y1="4.86" x2="-3.9" y2="3.8" width="0.0762" layer="21"/>
<wire x1="-3.9" y1="3.8" x2="-3.9" y2="2.2" width="0.0762" layer="51"/>
<wire x1="3.9" y1="4.86" x2="3.9" y2="3.8" width="0.0762" layer="21"/>
<wire x1="3.9" y1="3.8" x2="3.9" y2="2.2" width="0.0762" layer="51"/>
<wire x1="3.9" y1="0" x2="3.9" y2="2.2" width="0.0762" layer="21"/>
<wire x1="-3.9" y1="0" x2="-1.85" y2="0" width="0.0762" layer="21"/>
<wire x1="3.9" y1="0" x2="1.85" y2="0" width="0.0762" layer="21"/>
<wire x1="-0.15" y1="0" x2="0.15" y2="0" width="0.0762" layer="21"/>
<wire x1="-1.15" y1="0" x2="-0.85" y2="0" width="0.0762" layer="21"/>
<wire x1="0.85" y1="0" x2="1.15" y2="0" width="0.0762" layer="21"/>
<wire x1="-0.85" y1="0" x2="-0.15" y2="0" width="0.0762" layer="51"/>
<wire x1="0.15" y1="0" x2="0.85" y2="0" width="0.0762" layer="51"/>
<wire x1="1.15" y1="0" x2="1.85" y2="0" width="0.0762" layer="51"/>
<wire x1="-1.85" y1="0" x2="-1.15" y2="0" width="0.0762" layer="51"/>
<smd name="2" x="0.5" y="0" dx="1" dy="0.55" layer="1" rot="R90"/>
<smd name="1" x="1.5" y="0" dx="1" dy="0.55" layer="1" rot="R90"/>
<smd name="3" x="-0.5" y="0" dx="1" dy="0.55" layer="1" rot="R90"/>
<smd name="4" x="-1.5" y="0" dx="1" dy="0.55" layer="1" rot="R90"/>
<smd name="P$1" x="3.875" y="2.99" dx="1.26" dy="1.3" layer="1"/>
<smd name="P$2" x="-3.875" y="2.99" dx="1.26" dy="1.3" layer="1"/>
<rectangle x1="2.735" y1="3.56" x2="3.605" y2="4.81" layer="41"/>
<rectangle x1="-3.605" y1="3.56" x2="-2.735" y2="4.81" layer="41"/>
</package>
</packages>
<packages3d>
<package3d name="5037630491" urn="urn:adsk.eagle:package:32279485/2" type="model">
<packageinstances>
<packageinstance name="5037630491"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="M" urn="urn:adsk.eagle:symbol:13039056/3">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="MV" urn="urn:adsk.eagle:symbol:13039057/1">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="5037630491-BACKUP2" urn="urn:adsk.eagle:component:32286163/3" prefix="X">
<description>Pico-Lock 503763 Connector Header Surface Mount, Right Angle 4 position 0.039" (1.00mm)</description>
<gates>
<gate name="-2" symbol="M" x="5.08" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="5.08" y="-10.16" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="5.08" y="-12.7" addlevel="always" swaplevel="1"/>
<gate name="-1" symbol="MV" x="5.08" y="-5.08" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="5037630491">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:32279485/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="DIGIKEY" value="WM10655CT-ND"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="Signal" width="0.1524" drill="0.2">
<clearance class="0" value="0.1524"/>
</class>
</classes>
<parts>
<part name="U1" library="BMP390L" deviceset="BMP390L" device=""/>
<part name="C1" library="msrm" library_urn="urn:adsk.eagle:library:13039032" deviceset="GCM?R7*" device="155" package3d_urn="urn:adsk.eagle:package:23626/2" technology="1C104JA55"/>
<part name="X1" library="msrm" deviceset="5037630491-BACKUP2" device="" package3d_urn="urn:adsk.eagle:package:32279485/2"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="12.7" y="50.8" smashed="yes">
<attribute name="NAME" x="2.54" y="64.77" size="1.778" layer="95"/>
<attribute name="VALUE" x="2.54" y="39.37" size="1.778" layer="96" align="top-left"/>
</instance>
<instance part="C1" gate="G$1" x="33.02" y="33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="30.48" y="33.02" size="1.778" layer="95" rot="R180"/>
<attribute name="CAPACITANCE" x="30.48" y="35.56" size="1.778" layer="96" rot="R180"/>
<attribute name="VOLTAGE" x="30.48" y="38.1" size="1.778" layer="95" rot="R180"/>
</instance>
<instance part="X1" gate="-2" x="76.2" y="58.42" smashed="yes">
<attribute name="NAME" x="78.74" y="57.658" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-3" x="76.2" y="55.88" smashed="yes">
<attribute name="NAME" x="78.74" y="55.118" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-4" x="76.2" y="53.34" smashed="yes">
<attribute name="NAME" x="78.74" y="52.578" size="1.524" layer="95"/>
</instance>
<instance part="X1" gate="-1" x="76.2" y="60.96" smashed="yes">
<attribute name="NAME" x="78.74" y="60.198" size="1.524" layer="95"/>
<attribute name="VALUE" x="75.438" y="62.357" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="SCL" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCK"/>
<wire x1="-2.54" y1="50.8" x2="-12.7" y2="50.8" width="0.1524" layer="91"/>
<label x="-12.7" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-1" pin="S"/>
<wire x1="73.66" y1="60.96" x2="63.5" y2="60.96" width="0.1524" layer="91"/>
<label x="63.5" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VSS"/>
<wire x1="27.94" y1="43.18" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="33.02" y1="43.18" x2="38.1" y2="43.18" width="0.1524" layer="91"/>
<wire x1="33.02" y1="38.1" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
<junction x="33.02" y="43.18"/>
<label x="35.56" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-2" pin="S"/>
<wire x1="73.66" y1="58.42" x2="63.5" y2="58.42" width="0.1524" layer="91"/>
<label x="63.5" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SDI"/>
<wire x1="-2.54" y1="48.26" x2="-12.7" y2="48.26" width="0.1524" layer="91"/>
<label x="-12.7" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-3" pin="S"/>
<wire x1="73.66" y1="55.88" x2="63.5" y2="55.88" width="0.1524" layer="91"/>
<label x="63.5" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD/VDDIO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VDDIO"/>
<wire x1="27.94" y1="58.42" x2="38.1" y2="58.42" width="0.1524" layer="91"/>
<label x="35.56" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="33.02" y1="30.48" x2="33.02" y2="25.4" width="0.1524" layer="91"/>
<label x="33.02" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="SDO"/>
<wire x1="-2.54" y1="45.72" x2="-12.7" y2="45.72" width="0.1524" layer="91"/>
<label x="-12.7" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="CSB"/>
<wire x1="-2.54" y1="55.88" x2="-12.7" y2="55.88" width="0.1524" layer="91"/>
<label x="-12.7" y="55.88" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="27.94" y1="60.96" x2="38.1" y2="60.96" width="0.1524" layer="91"/>
<label x="35.56" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-4" pin="S"/>
<wire x1="73.66" y1="53.34" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<label x="63.5" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
