# Electronics

This repo contains all PCB designs regarding the OPENGRASP-LITE hand for Autodesk EAGLE. It contains the following PCB designs:

| Path | Description|
|---------|------|
|`ForceSensor/Fingertip`| The previous fingertip design only containing a barometer and with a connector
|`ForceSensor/Fingertip_Accel`| The new fingertip desing with barometer and accelerometer, as well as solder pads instead of a connector
|`ForceSensor/Hall_Touch`| A fingertip sensor using hall effect instead of a barometer
|`ForceSensor/Palm_0x76`| A barometer palm sensor adressable with the 0x76 I2C address
|`ForceSensor/Palm_0x77`| A barometer palm sensor adressable with the 0x77 I2C address
|`ForceSensor/Thumb`| A barometer sensor for the side of the thumb
|`Mainboard`| The mainboard, which is receiving the data from the sensors as well as talking to the EMG and Motorboard. Symetrically designed, so it can be used for both the left and right hand by simply flipping the PCB. Only the reset button must be soldered on the correct side.
|`MotorboardL`| The motorboard of the left hand, capable of driving five finger motors and a sixth one as a 2nd DOF for the thumb or one for the Palm
|`MotorboardR`| The motorboard of the right hand, capable of driving five finger motors and a sixth one as a 2nd DOF for the thumb or one for the Palm

Each of the files is designed according to the design rules of JLCPCB.
